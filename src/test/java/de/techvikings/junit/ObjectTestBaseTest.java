package de.techvikings.junit;

import org.junit.AssumptionViolatedException;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.Serializable;
import java.util.Iterator;
import java.util.function.Consumer;

import static java.util.Arrays.asList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests several cases which {@link ObjectTestBase} covers. The tests use mocking wherever possible.
 * For example, {@link #equals(Object)} and {@link #hashCode()} cannot be mocked and a dedicated
 * object is mocked manually.
 *
 * @author Matthaeus Mayer
 */
public class ObjectTestBaseTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    /**
     * Build runs try to execute all tests identified by {@code Test} annotation. Avoid this
     * execution!
     */
    @Ignore("Avoid execution of tests that become visible by extending the test base.")
    public static class ConcreteObjectTestBase
            extends ObjectTestBase<Object> {

        private final Object objectUnderTest;

        /**
         * @deprecated JUnit use only. If public no-args constructor is missing, the build run  is
         * complaining even if the whole tests is ignored.
         */
        @Deprecated
        public ConcreteObjectTestBase() {
            objectUnderTest = new Object();
        }

        public ConcreteObjectTestBase(Object objectUnderTest) {
            this.objectUnderTest = objectUnderTest;
        }

        @Override
        protected Object createObjectUnderTest() {
            return objectUnderTest;
        }
    }

    private static ObjectTestBase<Object> createObjectTestBase(final Object objectUnderTest) {
        return new ConcreteObjectTestBase(objectUnderTest);
    }

    private void expectSuccess(Object objectUnderTest, Consumer<ObjectTestBase> method) {
        final ObjectTestBase<Object> testUnderTest = createObjectTestBase(objectUnderTest);

        method.accept(testUnderTest);
    }

    private void expectFail(final Object objectUnderTest, final Consumer<ObjectTestBase> method,
                            final Class<? extends Throwable> throwableClass,
                            final String expectedMessage)
            throws Throwable {
        final ObjectTestBase<Object> testUnderTest = createObjectTestBase(objectUnderTest);

        expectedException.expect(throwableClass);

        if (expectedMessage != null) {
            expectedException.expectMessage(expectedMessage);
        }

        method.accept(testUnderTest);
    }

    @Test
    public void testSuccessIf_toString_returnsNonEmptyString() {
        final Object mock = mock(Object.class);

        when(mock.toString()).thenReturn("something useful");

        expectSuccess(mock, ObjectTestBase::testThat_toString_returnsSomething);
    }

    @Test
    public void testFailIf_toString_returnsNull() throws Throwable {
        final Object mock = mock(Object.class);

        when(mock.toString()).thenReturn(null);

        expectFail(mock, ObjectTestBase::testThat_toString_returnsSomething, AssertionError.class,
                   "expecting actual not to be null");
    }

    @Test
    public void testFailIf_toString_returnsEmptyString() throws Throwable {
        final Object mock = mock(Object.class);

        when(mock.toString()).thenReturn("");

        expectFail(mock, ObjectTestBase::testThat_toString_returnsSomething, AssertionError.class,
                   "expecting actual not to be empty");
    }

    @Test
    public void testFailIf_toString_throwsNullPointerException() throws Throwable {
        final Object mock = mock(Object.class);

        final NullPointerException npe = new NullPointerException();
        when(mock.toString()).thenThrow(npe);

        expectFail(mock, ObjectTestBase::testThat_toString_returnsSomething, npe.getClass(), null);
    }

    @Test
    public void testFailIf_equals_isMissing() throws Throwable {
        final Object mock = new Object() {
            @Override
            public int hashCode() {
                throw new UnsupportedOperationException();
            }
        };

        final Consumer<ObjectTestBase> method =
                ObjectTestBase::testThat_equals_and_hashcode_areEitherBothOrNonImplemented;
        expectFail(mock, method, AssertionError.class, "expected:<[tru]e> but was:<[fals]e>");
    }

    @Test
    public void testFailIf_hashCode_isMissing() throws Throwable {
        final Object mock = new Object() {
            @Override
            public boolean equals(Object obj) {
                throw new UnsupportedOperationException();
            }
        };

        final Consumer<ObjectTestBase> method =
                ObjectTestBase::testThat_equals_and_hashcode_areEitherBothOrNonImplemented;
        expectFail(mock, method, AssertionError.class, "expected:<[fals]e> but was:<[tru]e>");
    }

    @Test
    public void testSuccessIfBoth_hashCode_and_equals_arePresent() {
        final Object mock = new Object() {
            @Override
            public boolean equals(Object obj) {
                throw new UnsupportedOperationException();
            }

            @Override
            public int hashCode() {
                throw new UnsupportedOperationException();
            }
        };

        final Consumer<ObjectTestBase> method =
                ObjectTestBase::testThat_equals_and_hashcode_areEitherBothOrNonImplemented;
        expectSuccess(mock, method);
    }

    @Test
    public void testSuccessIfBoth_hashCode_and_equals_areMissing() {
        final Object mock = new Object() {
            // Implicitly a new class without equals(..) and hashCode()
        };

        final Consumer<ObjectTestBase> method =
                ObjectTestBase::testThat_equals_and_hashcode_areEitherBothOrNonImplemented;
        expectSuccess(mock, method);
    }

    @Test
    public void testFailIf_equals_returnsFalse_ifComparedToItself() throws Throwable {
        final Object mock = new Object() {
            @Override
            public boolean equals(Object obj) {
                return false;
            }
        };

        final Consumer<ObjectTestBase> method =
                ObjectTestBase::testThat_equals_returnsTrueWhenComparedToItself;
        expectFail(mock, method, AssertionError.class, "expected:<[tru]e> but was:<[fals]e>");
    }

    @Test
    public void testSuccessIf_equals_returnsTrue_ifComparedToItself() {
        final Object mock = new Object() {
            @Override
            public boolean equals(Object obj) {
                return true;
            }
        };

        expectSuccess(mock, ObjectTestBase::testThat_equals_returnsTrueWhenComparedToItself);
    }

    @Test
    public void testFailIf_equals_returnsTrue_ifArgumentNull() throws Throwable {
        final Object mock = new Object() {
            @Override
            public boolean equals(Object obj) {
                return true;
            }
        };

        expectFail(mock, ObjectTestBase::testThat_equals_isNullAware, AssertionError.class,
                   "expected:<[fals]e> but was:<[tru]e>");
    }

    @Test
    public void testSuccessIf_equals_returnsFalse_ifArgumentNull() {
        final Object mock = new Object() {
            @Override
            public boolean equals(Object obj) {
                return false;
            }
        };

        expectSuccess(mock, ObjectTestBase::testThat_equals_isNullAware);
    }

    @Test
    public void testFailIf_hashCode_isNotIdempotent() throws Throwable {
        final Object mock = new Object() {
            private Iterator<Integer> iterator = asList(123, 234).iterator();

            @Override
            public int hashCode() {
                return iterator.next();
            }
        };

        expectFail(mock, ObjectTestBase::testThat_hashCode_isIdempotent, AssertionError.class,
                   "expected:<[123]> but was:<[234]>");
    }

    @Test
    public void testSuccessIf_hashCode_isIdempotent() {
        final Object mock = new Object() {
            private Iterator<Integer> iterator = asList(123, 123).iterator();

            @Override
            public int hashCode() {
                return iterator.next();
            }
        };

        expectSuccess(mock, ObjectTestBase::testThat_hashCode_isIdempotent);
    }

    @Test
    public void testSkipOf_hashCode_comparisonForNonEqualObjects() throws Throwable {
        final Object mock = new Object() {
            @Override
            public boolean equals(Object obj) {
                return false;
            }
        };

        expectFail(mock, ObjectTestBase::testThat_hashCode_ofEqualObjectsReturnsTheSameHash,
                   AssumptionViolatedException.class, "got: <false>, expected: is <true>");
    }

    @Test
    public void testFailIf_hashCode_returnsDifferentHashCodesForEqualObjects() throws Throwable {
        final Object mock = new Object() {
            @Override
            public boolean equals(Object obj) {
                return true;
            }

            private Iterator<Integer> iterator = asList(123, 234).iterator();

            @Override
            public int hashCode() {
                return iterator.next();
            }
        };

        expectFail(mock, ObjectTestBase::testThat_hashCode_ofEqualObjectsReturnsTheSameHash,
                   AssertionError.class, "expected:<[123]> but was:<[234]>");
    }

    @Test
    public void testSuccessIf_hashCode_returnsEqualHashCodesForEqualObjects() {
        final Object mock = new Object() {
            @Override
            public boolean equals(Object obj) {
                return true;
            }

            private Iterator<Integer> iterator = asList(123, 123).iterator();

            @Override
            public int hashCode() {
                return iterator.next();
            }
        };

        expectSuccess(mock, ObjectTestBase::testThat_hashCode_ofEqualObjectsReturnsTheSameHash);
    }

    private static class CloneableObject
            implements Cloneable {
        @Override
        public Object clone() throws CloneNotSupportedException {
            return null;
        }
    }

    @Test
    public void testFailIf_clone_returnsNull() throws Throwable {
        final CloneableObject mock = new CloneableObject(); // clone returns null by default

        expectFail(mock, ObjectTestBase::testThat_clone_returnsEqualObject, AssertionError.class,
                   "expecting actual not to be null");
    }

    @Test
    public void testFailIf_clone_returnsNonEqualObject() throws Throwable {
        final CloneableObject mock = new CloneableObject() {
            @Override
            public boolean equals(Object obj) {
                return false;
            }

            @Override
            public Object clone() throws CloneNotSupportedException {
                return new CloneableObject();
            }
        };
        when(mock.toString()).thenReturn("cloneable");

        expectFail(mock, ObjectTestBase::testThat_clone_returnsEqualObject, AssertionError.class,
                   "expected:<[tru]e> but was:<[fals]e>");
    }

    @Test
    public void testSuccessIf_clone_returnsEqualObject() {
        final CloneableObject mock = new CloneableObject() {
            @Override
            public boolean equals(Object obj) {
                return true;
            }

            @Override
            public Object clone() throws CloneNotSupportedException {
                return new CloneableObject();
            }
        };
        when(mock.toString()).thenReturn("cloneable");

        expectSuccess(mock, ObjectTestBase::testThat_clone_returnsEqualObject);
    }

    private static class SerializableObject
            implements Serializable {
        private final boolean equals;

        public SerializableObject(boolean equals) {
            this.equals = equals;
        }

        @Override
        public boolean equals(Object obj) {
            return equals;
        }

        @Override
        public int hashCode() {
            return (equals ? 1 : 0);
        }
    }

    @Test
    public void testFailIfSerializationRoundTripLeadsToNonEqualObject() throws Throwable {
        final SerializableObject mock = new SerializableObject(false);

        expectFail(mock, ObjectTestBase::testThatSerializeRoundTrip, AssertionError.class,
                   "expected:<[tru]e> but was:<[fals]e>");
    }

    @Test
    public void testSuccessIfSerializationRoundTripLeadsToEqualObject() throws Throwable {
        final SerializableObject mock = new SerializableObject(true);

        expectSuccess(mock, ObjectTestBase::testThatSerializeRoundTrip);
    }

}

package de.techvikings.junit;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Objects;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.fest.assertions.api.Assertions.fail;
import static org.junit.Assume.assumeTrue;

/**
 * Basic class for all test cases testing a single object.
 *
 * @author Matthaeus Mayer
 */
public abstract class ObjectTestBase<O> {

    protected abstract O createObjectUnderTest();

    /**
     * Tests if {@link Object#toString()} is implemented to be called {@code null}-safe and provides
     * any useful information. Test is satisfied if no exception is thrown and the returned string
     * of {@link Object#toString()} is neither {@code null} nor empty.
     */
    @Test
    public void testThat_toString_returnsSomething() {
        final O object = createObjectUnderTest();
        assertThat(object.toString()).isNotNull()
                                     .isNotEmpty();
    }

    /**
     * Test that either non or both, {@link Object#equals(Object)} and {@link Object#hashCode()},
     * are implemented.
     */
    @Test
    public void testThat_equals_and_hashcode_areEitherBothOrNonImplemented() {
        final O object = createObjectUnderTest();
        final Class objectClass = object.getClass();
        final boolean equalsIsImplemented = equalsIsImplementedIn(objectClass);
        final boolean hashCodeIsImplemented = hashCodeIsImplementedIn(objectClass);
        assertThat(equalsIsImplemented).isEqualTo(hashCodeIsImplemented);
    }

    protected static boolean equalsIsImplementedIn(Class objectClass) {
        return getDeclaredMethod(objectClass, "equals", new Class[]{Object.class}) != null;
    }

    protected static boolean hashCodeIsImplementedIn(Class objectClass) {
        return getDeclaredMethod(objectClass, "hashCode", null) != null;
    }

    private static Method getDeclaredMethod(Class<?> objectClass, String methodName,
                                            Class<?>[] methodParameters) {
        try {
            return objectClass.getDeclaredMethod(methodName, methodParameters);
        } catch (NoSuchMethodException ignore) {
            return null;
        }
    }

    /**
     * Test is satisfied if an object's {@link Object#equals(Object)}-method returns {@code true} if
     * it is compared to itself.
     */
    @Test
    public void testThat_equals_returnsTrueWhenComparedToItself() {
        final O object = createObjectUnderTest();
        assertThat(object.equals(object)).isTrue();
    }

    /**
     * Test is satisfied if an object's {@link Object#equals(Object)}-method returns {@code false}
     * if it is compared to {@code null}.
     */
    @Test
    public void testThat_equals_isNullAware() {
        final O object = createObjectUnderTest();
        assertThat(object.equals(null)).isFalse();
    }

    /**
     * Test calls {@link Object#hashCode()} two times of one object. The test succeeds if both
     * results are equal and hence the implementation is idempotent.
     */
    @Test
    public void testThat_hashCode_isIdempotent() {
        final O object = createObjectUnderTest();
        final int expected = object.hashCode();
        final int actual = object.hashCode();
        assertThat(actual).isEqualTo(expected);
    }

    /**
     * Tests if {@link Object#hashCode()} of equal but not same objects returns equal values.<p>
     *
     * <b>Note:</b> If the objects are not equal the test stops without testing the values.
     */
    @Test
    public void testThat_hashCode_ofEqualObjectsReturnsTheSameHash() {
        final O object1 = createObjectUnderTest();
        final O object2 = createObjectUnderTest();

        assumeTrue(object1.equals(object2));

        final int expected = object2.hashCode();
        final int actual = object1.hashCode();
        assertThat(actual).isEqualTo(expected);
    }

    /**
     * If an object implements {@link Cloneable} the test tries to find the clone method via
     * reflection.<p>
     *
     * Test is satisfied if the clone is not {@code null}. Additionally, if {@link
     * Object#equals(Object)} is overridden in the object's class the test continues asserting that
     * the clone is equal to its original .
     */
    @Test
    public void testThat_clone_returnsEqualObject() {
        final O original = createObjectUnderTest();
        assumeTrue(original instanceof Cloneable);

        final Class objectClass = original.getClass();

        try {
            final Method cloneMethod = findMethod(objectClass, "clone");

            if (!cloneMethod.isAccessible()) {
                cloneMethod.setAccessible(true);
            }

            final Object clone = cloneMethod.invoke(original);

            assertThat(clone).isNotNull();

            assumeTrue(equalsIsImplementedIn(objectClass));
            assertThat(original.equals(clone)).isTrue();
        } catch (InvocationTargetException e) {
            fail("clone() not supported", e);
        } catch (NoSuchMethodException e) {
            fail("clone() not found", e);
        } catch (IllegalAccessException e) {
            fail("clone() not accessible", e);
        }
    }

    protected Method findMethod(Class objectClass, String methodName) throws NoSuchMethodException {
        Objects.requireNonNull(objectClass, "objectClass");
        Objects.requireNonNull(methodName, "methodName");

        Class<?> currentClass = objectClass;
        while (currentClass != null) {
            try {
                return currentClass.getDeclaredMethod(methodName);
            } catch (NoSuchMethodException e) {
                // Not found in current class
            }

            currentClass = currentClass.getSuperclass();
        }

        throw new NoSuchMethodException(methodName + " in class " + objectClass.getName());
    }

    /**
     * This test performs a round-trip through the lifecycle of a serializable object and is
     * satisfied if the result is equal to the initially created object. The round-trip consists of
     * the following steps:
     *
     * <ol>
     *
     * <li>Creation step: Instantiation of the object under test.</li>
     *
     * <li>Serialization step: Writing the object into an {@link ObjectOutputStream}.</li>
     *
     * <li>Deserialization step: Reading the resulting bytes from serialization step via {@link
     * ObjectInputStream}.</li>
     *
     * <li>Assertion: Check for equality of both, the original and the deserialized object.</li>
     *
     * </ol>
     *
     * The test is satisfied if the deserialized object ist not {@code null}. Additionally, if the
     * object provides an own implementation of the {@link Object#equals(Object)}-method both
     * objects, the original and the deserialized, are compared to each other. If {@link
     * Object#equals(Object)} returns {@code true} the test is finally satisfied.
     */
    @Test
    public void testThatSerializeRoundTrip() {
        final O original = createObjectUnderTest();
        assumeTrue(original instanceof Serializable);

        final ByteArrayOutputStream sink = new ByteArrayOutputStream();
        try (final ObjectOutputStream objectOutputStream = new ObjectOutputStream(sink)) {
            objectOutputStream.writeObject(original);
        } catch (IOException e) {
            fail("IOException while serializing", e);
        }

        Object result = null;
        final ByteArrayInputStream source = new ByteArrayInputStream(sink.toByteArray());
        try (final ObjectInputStream inputStream = new ObjectInputStream(source)) {
            result = inputStream.readObject();
        } catch (IOException e) {
            fail("IOException while deserializing", e);
        } catch (ClassNotFoundException e) {
            fail("ClassNotFoundException while deserializing", e);
        }

        assertThat(original).isNotNull();

        assumeTrue(equalsIsImplementedIn(original.getClass()));
        assertThat(original.equals(result)).isTrue();
    }
}
